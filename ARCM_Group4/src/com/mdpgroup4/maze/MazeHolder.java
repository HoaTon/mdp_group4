package com.mdpgroup4.maze;

import android.view.View;
import android.widget.FrameLayout;

import com.mdpgroup4.robot.Robot;

/*
 * MazeHolder interface. Interface define behaviors of a maze renderer
 */

public interface MazeHolder {
	public void bind(Maze maze);
	public void bind(Robot robot);
	public void bind(FrameLayout frameLayout);
    public void onDataChanged();

	public Maze 	getMaze();
	public Robot 	getRobot();
	public View	 	getView();
	public void		preview();
}
