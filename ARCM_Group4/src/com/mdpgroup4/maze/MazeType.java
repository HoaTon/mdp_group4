package com.mdpgroup4.maze;

public enum MazeType {
	BASIC(0),
	ADVANCED(1);
	
	public final int _id;
	
	private MazeType(int id){
		_id = id;
	}
}
