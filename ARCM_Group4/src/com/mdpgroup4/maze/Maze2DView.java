
package com.mdpgroup4.maze;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;

/**
 * Created by Hoa_2 on 2/24/14.
 */

public class Maze2DView extends View {



    public Maze2DView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Maze2DView(Context context) {
        this(context, null);
    }

    Board   mBoard;
    RectF   mGridBound;
    RectF   mBoardBound;
    Paint   mBoardPaint;
    int     mBoardColor;

    Paint   mGridPaint;
    int     mGridColor;
    int     mObstacleColor;
    int     mUnknownColor;
    RectF   mRobotBound;
    Paint   mRobotPaint;
    int     mRobotColor;

    Robot   mRobot;
    private void init(){
        //set board size
        mBoard = new Board();
        mBoard.width            = 8;   //TODO: demo data.
        mBoard.height           = 8;
        mBoard.gridPaddingRatio = 0.1f;    //dummy data
        mBoard.gridSize          = 10.0f;     //dummy data
        mBoard.gridPadding       = 1.0f;     //dummy data
        mBoard.setGridState(3, 4, GridState.CLEAR);
        mBoard.setGridState(3, 5, GridState.CLEAR);
        mBoard.setGridState(4, 5, GridState.CLEAR);
        mBoard.setGridState(5, 6, GridState.CLEAR);
        mBoardBound = new RectF();
        mGridBound = new RectF();

        mBoard.invalidate();

        //set board paint
        mBoardPaint     = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBoardColor     = Color.parseColor("#FFFFFF");
        mBoardPaint.setColor(mBoardColor);

        mGridPaint      = new Paint(Paint.ANTI_ALIAS_FLAG);


        mGridColor      = Color.parseColor("#E5E1D1");
        mObstacleColor  = Color.parseColor("#2C343B");
        mUnknownColor   = Color.parseColor("#B2B2A8");
        mGridPaint.setColor(mGridColor);

        //set robot
        mRobot = new Robot();
        mRobot.width = 3;
        mRobot.height= 3;
        mRobot.posX = 0;
        mRobot.posY = 0;
        mRobot.direction = Direction.NORTH;
        mRobotBound = new RectF();
        mRobotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRobotColor = Color.parseColor("#C44741");

        mRobotPaint.setColor(mRobotColor);
        mRobotPaint.setStrokeWidth(2.0f);
        mRobotPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        onDataChanged();
    }

    private void onDataChanged() {

        //update map size and obstacle map
        if (!mBoard.isValid()){
            Pair  innerBoardSize     = mBoard.getInnerBoardSize();
            float padding            = mBoard.getBoardMargin();
            mGridBound = new RectF(padding,
                                   padding,
                                   padding+(Float)innerBoardSize.first,
                                   padding+(Float)innerBoardSize.second);
            mBoard.update();
        }

        //update robot position
        Pair robotCoords = mBoard.getGridCoordinate(mRobot.posX,mRobot.posY-1); //+1 to move robot center to bottom left instead top left
        mRobotBound = new RectF((Float)robotCoords.first    -mBoard.gridSize/2,
                                (Float)robotCoords.second   -mBoard.gridSize/2,
                                (Float)robotCoords.first    +mRobot.width*mBoard.gridSize/2,
                                (Float)robotCoords.second   +mRobot.height*mBoard.gridSize/2);
        //invalidate view to force refresh
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Try for a width based on our minimum
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        float xpad = (float) (getPaddingLeft() + getPaddingRight());
        float ypad = (float) (getPaddingTop() + getPaddingBottom());

        float ww = (float)w - xpad;
        float hh = (float)h - ypad;

        mBoard.gridSize     = Math.min( ww/ (float)(mBoard.width  + 2*mBoard.gridMarginRatio),
                                        hh/ (float)(mBoard.height + 2*mBoard.gridMarginRatio))
                            / (1+ mBoard.gridPaddingRatio);
        mBoard.gridPadding  = mBoard.gridSize*mBoard.gridPaddingRatio;
        mBoard.invalidate();

        onDataChanged();
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);

        //draw board
        canvas.drawRect(mGridBound, mBoardPaint);

        //draw grid
        float x = mGridBound.left;
        float y = mGridBound.top;

        for (int j=0;j<mBoard.height;j++){
            y += mBoard.gridPadding/2;

            for (int i=0;i<mBoard.width;i++){
                x += mBoard.gridPadding/2;

                if (mBoard.isObstacle(i,j))
                    mGridPaint.setColor(mObstacleColor);//change to obstacle color

                else if (mBoard.isUnknown(i, j))
                    mGridPaint.setColor(mUnknownColor);//change to obstacle color
                canvas.drawRect(x,y,x+mBoard.gridSize,y+mBoard.gridSize,mGridPaint);
                //canvas.drawRect(x,y,x+mBoard.gridSize,y+mBoard.gridSize,mGridPaint); trick part. reverse y-axis on render
                mGridPaint.setColor(mGridColor);        //reset grid paint color

                x += mBoard.gridPadding/2;
                x += mBoard.gridSize;
            }
            y += mBoard.gridSize;
            y += mBoard.gridPadding/2;
            x = mGridBound.left;
        }

        //draw robot
        canvas.drawPath(createRobotPath(), mRobotPaint);
    }

    private Path createRobotPath(){
        Path path = new Path();
        //draw robot body
        path.setFillType(Path.FillType.EVEN_ODD);
        path.addRect(mRobotBound, Path.Direction.CW);

        //draw direction arrow
        float arrowLength = mBoard.gridSize * 0.6f;
        float arrayHeadRadius = 5f;

        PointF centerPoint = new PointF(mRobotBound.centerX(),mRobotBound.centerY());
        PointF endPoint = new PointF(centerPoint.x + mRobot.direction.x*(mBoard.gridSize*mRobot.width/2+arrowLength),
                                     centerPoint.y + mRobot.direction.y*(mBoard.gridSize*mRobot.height/2+arrowLength));

        path.moveTo(centerPoint.x,centerPoint.y);
        path.lineTo(endPoint.x, endPoint.y);
        path.addCircle(endPoint.x,endPoint.y,arrayHeadRadius,Path.Direction.CW);
        path.close();
        return path;
    }

    class Robot{
        int posX;
        int posY;
        int width;
        int height;
        Direction direction;
    }

    class Board{
        boolean valid;
        int     width;
        int     height;
        float   gridSize;
        float   gridPadding;
        float   gridPaddingRatio;     //ratio of gridPadding/gridSize
        int     gridMarginRatio;    //board margin, calculated in number of grid
        GridState[][] state;

        public Board(){
            width = 10;     //default value
            height = 10;
            gridMarginRatio = 1;
            gridPaddingRatio = 0.1f;
            valid = true;
            update();
        }

        public void setGridState(int x, int y, GridState state){
            this.state[y][x] = state;
        }

        public GridState getGridState(int x, int y) {
            return this.state[y][x];
        }

        public boolean isObstacle(int x, int y){
            if (state[y][x] == GridState.OBSTACLE)
                return true;
            return false;
        }

        public Pair getGridCoordinate(int x, int y){
            return new Pair<Float,Float>((gridSize+gridPadding)*(x+ gridMarginRatio +0.5f),
                                         (gridSize+gridPadding)*(y+ gridMarginRatio +0.5f));
        }

        public void invalidate() {
            valid = false;
        }

        public boolean isValid() {
            return valid;
        }

        public void update(){
            //update state
            if (state == null){
                state = new GridState[height][width];
                for (int j=0;j<height;j++)
                    for (int i=0;i<width;i++)
                        state[j][i] = GridState.CLEAR;
            } else {
                GridState[][] tempMap = state;
                state = new GridState[height][width];
                for (int i=0;i<height;i++)
                    for (int j=0;j<width;j++){
                        if (i<tempMap.length){
                            if (j<tempMap[i].length)
                                state[i][j] = tempMap[i][j];
                        } else
                            state[i][j] = GridState.CLEAR;
                    }

            }
            valid = true;
        }

        public Pair getInnerBoardSize() {
            return new Pair<Float,Float>((gridSize+ gridPadding) * width,(gridSize+ gridPadding) * height);
        }

        public Pair getOuterBoardSize() {
            return new Pair<Float,Float>((gridSize+ gridPadding) * (width+ gridMarginRatio),
                                         (gridSize+ gridPadding) * (height+ gridMarginRatio));
        }

        public float getBoardMargin(){
            return (gridSize+ gridPadding) * gridMarginRatio;
        }


        public boolean isUnknown(int x, int y) {
            if (state[y][x] == GridState.UNKNOWN)
                return true;
            return false;

        }
    }

    protected enum Direction{
        NORTH(0,-1),EAST(1,0),SOUTH(0,1),WEST(-1,0);
        public final int x;
        public final int y;

        private Direction(int x, int y){
            this.x = x;
            this.y = y;
        }
    }

    protected enum GridState{
        CLEAR(0),OBSTACLE(1),UNKNOWN(2);

        public final int mState;

        private GridState(int state){
            this.mState = state;
        }

        public static GridState state(int gridState) {
            switch (gridState){
                case Maze.BLOCKED:
                    return GridState.OBSTACLE;

                case Maze.CLEAR:
                    return GridState.CLEAR;

                case Maze.UNKNOWN:
                    return GridState.UNKNOWN;

                default:
                    return GridState.CLEAR;
            }
        }
    }

    //public API
    protected void setGridState(int x, int y,GridState state){
        y = mBoard.height-y-1;
        mBoard.setGridState(x,y,state);
        onDataChanged();
    }

    protected GridState getGridState(int x, int y){
        y = mBoard.height - y-1;
        return mBoard.getGridState(x, y);
    }

    protected void setRobotState(int x, int y, Direction dir){
        mRobot.posX = x;
        mRobot.posY = mBoard.height-y-1;
        mRobot.direction = dir;
        onDataChanged();
    }

    protected void setBoardSize(int w, int h){
        mBoard.width = w;
        mBoard.height = h;
        mBoard.invalidate();
        onDataChanged();
    }
}


