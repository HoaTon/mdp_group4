package com.mdpgroup4.maze;

import android.util.Log;

import com.mdpgroup4.controller.DefaultSetting;

public class Maze {

    public static final int UNKNOWN = 2;
	public static final int CLEAR = 0;
	public static final int BLOCKED = 1;
	
	private int         _width;
	private int         _height;
	private boolean     _valid;

	private int[][] _value ;
	public Maze(){
		_width 	= 	DefaultSetting.MAZE_WIDTH;
		_height	= 	DefaultSetting.MAZE_HEIGHT;
		
		_value = new int[_height][_width];
		for (int i = 0;i<_width;i++)
			for (int j = 0;j<_height;j++)
				_value[j][i] = CLEAR;
        _valid=false;
	}
	
	public int getGridState(int x, int y){
		if (isValidCoordinate(x,y))
			return _value[y][x];
		return -1;
	}
	
	public void setGridState(int x, int y, int state){
		if (isValidCoordinate(x,y)){
			if (state == CLEAR || state == BLOCKED ||state == UNKNOWN)
			_value[y][x] = state;
		}
        _valid = false;
	}
	
	private boolean isValidCoordinate(int x, int y){
		return (x>=0 && y>=0 && x<_width && y<_height);
	}

    public int getWidth(){
        return _width;
    }

    public int getHeight(){ return _height;}

    public boolean isValid(){
        return _valid;
    }

    public void setValid(boolean value){
        _valid = value;
    }
}
