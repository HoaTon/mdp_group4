package com.mdpgroup4.maze;

import com.mdpgroup4.robot.Robot;

import android.content.Context;
import android.util.Log;
import android.widget.FrameLayout;

/*
 * dummy class
 */
public class BasicMazeHolder implements MazeHolder{

	private static final String _TAG	= "com.mdpgroup4.maze.BasicMazeHolder";
	private Context             _context = null;
	private Maze 				_maze = null;
	private Robot 				_robot = null;
	private Maze2DView 		    _view = null;
    private FrameLayout         _parent = null;
    public BasicMazeHolder(Context context) {
        _context = context;
        init();
    }

    private void init() {
        _view = new Maze2DView(_context);
    }

    public void bind(Maze maze){
		Log.v(_TAG,"bind(): binding maze to mazeholder");
		_maze = maze;
        _view.setBoardSize(_maze.getWidth(),_maze.getHeight());
        onDataChanged();
	}
	
	public void bind(Robot robot){
		Log.v(_TAG,"bind(): binding robot to mazeholder");
		_robot = robot;
        onDataChanged();
	}

    public void onDataChanged() {
        if (_maze!=null){
            if (!_maze.isValid()){
                for (int j=0;j<_maze.getHeight();j++){
                    for (int i=0;i<_maze.getWidth();i++){
                        _view.setGridState(i,j,Maze2DView.GridState.state(_maze.getGridState(i,j)));
                    }
                }
            }
            _maze.setValid(true);
        }
        if (_robot!=null){
            if (!_robot.isValid()){
                Maze2DView.Direction robotDir;
                switch (_robot.getDirection()){
                    case DIRECTION_N:
                        robotDir = Maze2DView.Direction.NORTH;
                        break;
                    case DIRECTION_E:
                        robotDir = Maze2DView.Direction.EAST;
                        break;
                    case DIRECTION_S:
                        robotDir = Maze2DView.Direction.SOUTH;
                        break;
                    case DIRECTION_W:
                        robotDir = Maze2DView.Direction.WEST;
                        break;
                    default:
                        robotDir = Maze2DView.Direction.EAST;
                }
                _view.setRobotState(_robot.getPosX(),_robot.getPosY(),robotDir);
                _view.invalidate();
            }
            _robot.setValid(true);
        }
    }

    /**
	 * Expect view as a FrameLayout
     * @param frameLayout
     */

	public void bind(FrameLayout frameLayout) {
		if (frameLayout ==null)
			return;
		Log.v(_TAG,"bind(): binding placeholder to mazeholder");
		
		_parent = frameLayout;

	}
	
	public Maze getMaze(){
		return _maze;
	}
	
	public Robot getRobot(){
		return _robot;
	}

	public Maze2DView getView() {
		return _view;
	}

	public void preview() {

		//TODO: demo, to be updated
		if (!(_maze!=null && _robot!=null && _parent!=null)){
			Log.e(_TAG,"preview() : missing compulsory binding(s).");
			return;
		}
        if (_parent.getChildCount()==0){
            _parent.addView(_view,
                    new FrameLayout.LayoutParams(   FrameLayout.LayoutParams.MATCH_PARENT,
                                                    FrameLayout.LayoutParams.MATCH_PARENT)
            );
        }
        _parent.invalidate();
        _parent.requestLayout();
	}
}
