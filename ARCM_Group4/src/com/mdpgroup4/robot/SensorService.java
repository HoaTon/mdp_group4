package com.mdpgroup4.robot;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.mdpgroup4.activities.MainActivity;
import com.mdpgroup4.controller.KeyMapper;

import java.lang.ref.WeakReference;
import java.util.List;

public class SensorService extends Service {

    private static final String _TAG = "SensorService";

    //Message identifier
    public static final int MESSAGE_SENSOR = 0;

    //control commands
    public static final int TURN_LEFT = 0;
    public static final int TURN_RIGHT = 1;
    public static final int FORWARD = 2;
    public static final int REVERSE = 3;
    //constant variable for onChanged

    public static boolean listening = false;
    public boolean callMade = false;
    //comment here
    private Sensor mAcc;
    private SensorManager mSensorManager;

    private static final float THRESHOLD_X = (float) 3.5;
    private static final float THRESHOLD_Y = (float) 3.5;
    private static final int TIME_DELAY = 1000;

    private Messenger mMessenger;

    String xyz = null;
    long enableTime;
    float deltaX;
    float deltaY;
    float latestX;
    float latestY;


    private SensorEventListener accelEventListener =
            new SensorEventListener() {

        public void onAccuracyChanged(Sensor sensor, int accuracy) {};

        public final void onSensorChanged(SensorEvent event) {
            // The light sensor returns a single value.
            // Many sensors return 3 values, one for each axis.
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            deltaX = Math.abs(x-latestX);
            deltaY = Math.abs(y-latestY);

            //landscape

            if (System.currentTimeMillis() > enableTime + TIME_DELAY
                    || deltaX > THRESHOLD_X || deltaY > THRESHOLD_Y){
                xyz = "X: " + x + "\n" + "Y: " + y + "\n" + "Z: " + z;

                if (z >= 0){
                    if (-THRESHOLD_X <= y && y <= THRESHOLD_X
                            && -THRESHOLD_Y <= x && x <= THRESHOLD_Y){
                    } else {
                        if (x > THRESHOLD_X) {
                            sendMessage(KeyMapper.MOVE_LEFT_FUNC);
                        }
                        if (x < -THRESHOLD_X) {
                            sendMessage(KeyMapper.MOVE_RIGHT_FUNC);
                        }
                        if (y > THRESHOLD_Y) {
                            sendMessage(KeyMapper.MOVE_DOWN_FUNC);
                        }
                        if (y < -THRESHOLD_Y) {
                            sendMessage(KeyMapper.MOVE_UP_FUNC);
                        }
                    }
                }
                enableTime = System.currentTimeMillis();
            }

            latestX = x;
            latestY = y;

        }
    };

    private void sendMessage(int code) {
        Message msg = Message.obtain(null,MESSAGE_SENSOR,-1,-1,code);
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            Log.e(_TAG,"sendMessage()" + e.getMessage());
            e.printStackTrace();
        }

    }

    public void startListener(){
    mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
    List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
    if (sensors.size() > 0)
    {
    mAcc = sensors.get(0);
    listening = mSensorManager.registerListener(accelEventListener, mAcc, SensorManager.SENSOR_DELAY_GAME);

    }
}

    public class TiltBinder<S> extends Binder{

        private WeakReference<S> mService;

        public TiltBinder (S service)
        {
            mService = new WeakReference<S>(service);
        }

        public S getService()
        {
            return mService.get();
        }
    }


    public IBinder mBinder;
    @Override
    public void onCreate()
    {
        mBinder = new TiltBinder<SensorService>(this);
    }

    public boolean isListening()
    {
        return listening;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        //get Messenger from MainActivity
        mMessenger = intent.getParcelableExtra(TiltSensingController.MESSENGER);
        startListener();
        return SensorService.START_STICKY;
    }

    @Override
    public void onDestroy()
    {
        if (listening)
            stopListening();
        mBinder = null;
        super.onDestroy();
    }


    private void stopListening()
    {

        listening = false;
        try {
            if (mSensorManager != null && accelEventListener != null)
            {
                mSensorManager.unregisterListener(accelEventListener);
            }
        } catch (Exception e) {}
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }
}