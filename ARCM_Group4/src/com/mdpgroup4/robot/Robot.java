package com.mdpgroup4.robot;

import com.mdpgroup4.controller.DefaultSetting;

public class Robot {

	public enum State{
		NOT_READY(-1, "Not Ready"),
		IDLE(0,"Idle"),
		MOVING(1,"Moving"),
        TURNING(2,"Turning");
		
		public final int 		_id;
		public final String 	_desc;
		
		private State(int id, String desc){
			_id = id;
			_desc = desc;
		}		
	}

    public enum Direction {
        DIRECTION_N(0), DIRECTION_E(1), DIRECTION_S(2), DIRECTION_W(3);
        public final int value;

        Direction(int value) {
            this.value = value;
        }
    }

    private int 	_posX;
	private int 	_posY;
	private Direction _direction;
	private State 	_state;
	private boolean _valid;

	public Robot(){		//default
		_posX = DefaultSetting.ROBOT_X;
		_posY = DefaultSetting.ROBOT_Y;
		_direction = DefaultSetting.ROBOT_DIRECTION;	
		_state = State.IDLE;
        _valid = false;
	}

	public int getPosX() {
		return _posX;
	}

	public void setPosX(int posX) {
		this._posX = posX;
	}

	public int getPosY() {
		return _posY;
	}

	public void setPosY(int posY) {
		this._posY = posY;
        _valid = false;
	}

	public Direction getDirection() {
		return _direction;
	}

	public void setDirection(Direction direction) {
		this._direction = direction;
        _valid = false;
	}

	public State getState() {
		return _state;
	}

	public void setState(State state) {
		this._state = state;
        _valid = false;
	}

    public boolean isValid(){return _valid;}

    public void setValid(boolean value){_valid = value; }
}
