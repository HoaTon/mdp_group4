package com.mdpgroup4.robot;

/*
 * dummy class
 */
public interface NavigationController{
    /**
     * Get control command from input signal.
     *
     *
     * @param args
     * @return
     */
	public String getControlCmd(String args[]);
    public String getControlCmd(int functionId);
}
