package com.mdpgroup4.robot;

import android.util.Log;

import com.mdpgroup4.controller.KeyMapper;
import com.mdpgroup4.controller.MainController;

import java.util.HashMap;

public class BasicNavController implements NavigationController{

    private static final String mTAG = "com.mdpgroup4.controller.navigationcontroller.BasicNavController";

    private int[]     internalKeyMap;
    private int[]     reversedKeyMap;
    private boolean   internalKeyMapping;

    private HashMap<Robot.Direction,Integer>         internalDirectionMap ;

    public String getControlCmd(String[] args) {
        if (args.length>0)
            if (args[0].trim()!=null){
                int parsedCmd = Integer.parseInt(args[0]);
                return getControlCmd(parsedCmd);
            }
        return null;
    }

    @Override
    public String getControlCmd(int functionId) {
        String cmd = "";
        int parsedCmd = functionId;
        if (internalKeyMapping){
            cmd = KeyMapper.getValue(
                    reMapKeyId(parsedCmd,MainController.getInstance().getRobot()
                            .getDirection()
                    )
            );
        } else{
            cmd =  KeyMapper.getValue(parsedCmd);
        }
        return cmd;
    }

    public BasicNavController(){
        init();
        internalKeyMapping = true; // set default to true
    }

    private void init() {
        //init internal key map
        internalKeyMap = new int[4];
        internalKeyMap[0] = KeyMapper.MOVE_UP_FUNC;
        internalKeyMap[1] = KeyMapper.MOVE_RIGHT_FUNC;
        internalKeyMap[2] = KeyMapper.MOVE_DOWN_FUNC;
        internalKeyMap[3] = KeyMapper.MOVE_LEFT_FUNC;

        //reversed key map
        reversedKeyMap = new int[4];
        reversedKeyMap[KeyMapper.MOVE_UP_FUNC] = 0;
        reversedKeyMap[KeyMapper.MOVE_RIGHT_FUNC] = 1;
        reversedKeyMap[KeyMapper.MOVE_DOWN_FUNC] = 2;
        reversedKeyMap[KeyMapper.MOVE_LEFT_FUNC] = 3;


        //init direction map
        internalDirectionMap = new HashMap<Robot.Direction, Integer>(10);
        internalDirectionMap.put(Robot.Direction.DIRECTION_N, 0);
        internalDirectionMap.put(Robot.Direction.DIRECTION_E, 1);
        internalDirectionMap.put(Robot.Direction.DIRECTION_S, 2);
        internalDirectionMap.put(Robot.Direction.DIRECTION_W, 3);
    }

    //remap keyId base on robot direction
    private int reMapKeyId(int actualKeyId, Robot.Direction dir){
        return internalKeyMap[(reversedKeyMap[actualKeyId] - internalDirectionMap.get(dir)+4)%4];

    }

//    private int reMapKeyId(String actualKeyId, Robot.Direction dir){
//        return reMapKeyId(Integer.parseInt(actualKeyId),dir);
//    }

    public boolean isInternalKeyMapping() {
        return internalKeyMapping;
    }

    public void setInternalKeyMapping(boolean internalKeyMapping) {
        this.internalKeyMapping = internalKeyMapping;
    }
}
