package com.mdpgroup4.robot;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Hoa_2 on 3/10/14.
 */


public class TiltSensingController extends BasicNavController{

    public static final String MESSENGER = "Messenger";
    private static final String _TAG = "TiltSensingController";

    SensorService   mSensorService;
    Boolean         mIsBound;
    Messenger       mMessenger;
    Context         mContext;

    public TiltSensingController(Context context, Handler handler){
        super();
        mContext = context;
        mMessenger = new Messenger(handler);
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            mSensorService = ((SensorService.TiltBinder<SensorService>)service).getService();
            Toast.makeText(mContext, "Service Connected",
                    Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            mSensorService = null;
            Toast.makeText(mContext,"Service Disconnected",
                    Toast.LENGTH_SHORT).show();
        }
    };

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).
        mContext.bindService(new Intent(mContext,
                SensorService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            mContext.unbindService(mConnection);
            mIsBound = false;
        }
    }

    public void toggleSensorService(){
        if (mSensorService == null){
            try {
                //start sensor tracking service
                Intent serviceIntent = new Intent(mContext, SensorService.class);
                serviceIntent.putExtra(MESSENGER, mMessenger);
                mContext.startService(serviceIntent);

                doBindService();
            } catch (Exception e){
                //cannot start sensor service
            }

        } else {
            doUnbindService();
            mSensorService.stopSelf();
            mSensorService = null;
        }
    }

    @Override
    public String getControlCmd(String[] args) {
        if (args == null){
            toggleSensorService();
            return null;
        }else
            return super.getControlCmd(args);

    }

}
