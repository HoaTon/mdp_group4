package com.mdpgroup4.robot;


public enum NavControllerType {
	BASIC(0),
	ADVANCED(1);
	
	public final int _id;
	
	private NavControllerType(int id){
		_id = id;
	}
}
