package com.mdpgroup4.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import com.mdpgroup4.main.R;

public class PreferenceSetting extends PreferenceActivity implements
        OnSharedPreferenceChangeListener{


    Context mContext = null;  
 
    @Override  
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.layout.setconfig);
        SharedPreferences sp = getPreferenceScreen().getSharedPreferences();

 
        mContext = this;
 
        // EditTextPreference
        EditTextPreference mEditText1 = (EditTextPreference) findPreference("text_0");
        EditTextPreference mEditText2 = (EditTextPreference) findPreference("text_1");

        //update Summary
        mEditText1.setSummary(sp.getString("text_0", "Click to change"));
        mEditText2.setSummary(sp.getString("text_1", "Click to change"));

        //Set Dialog
        mEditText1.setPositiveButtonText("Confirm");
        mEditText1.setNegativeButtonText("Cancel");
        mEditText2.setPositiveButtonText("Confirm");
        mEditText2.setNegativeButtonText("Cancel");

    }

    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        Preference pref = findPreference(key);
        if (pref instanceof EditTextPreference) {
            EditTextPreference etp = (EditTextPreference) pref;
            pref.setSummary(etp.getText());
        }
        this.setResult(Activity.RESULT_OK);
    }

    public static String getPref(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }
    //put under menu button
    //to be done
}
