package com.mdpgroup4.activities;

import com.mdpgroup4.controller.KeyMapper;
import com.mdpgroup4.controller.MainController;
import com.mdpgroup4.main.R;
import com.mdpgroup4.networkservice.BluetoothService;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
//import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
//import android.view.View;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
//import android.widget.Button;
//import android.widget.EditText;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * This is the main Activity that displays the current chat session.
 */
public class MainActivity extends Activity {
    // Debugging
    private static final String _TAG = "MainActivity";
    //private static final String _TAG	= "com.mdpgroup4.main.MainActivity";
    private static final boolean D = true;


    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_ROBOT_STATE_CHANGE = 6;

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_SET_CONFIGURATION = 0;
    private static final int REQUEST_TILTING_CONTROL = 3;


    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter

    // Member object for the chat services
    private boolean             tiltSensingMode = false;
    private BluetoothService    mChatService = null;
    private MainController      _mainController	= null;

    TextView robotStatus;
    TextView bluetoothStatus;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(D) Log.e(_TAG, "+++ ON CREATE +++");

        setContentView(R.layout.activity_main);
        initialize();

        robotStatus = (TextView) findViewById(R.id.RobotStatus);
        bluetoothStatus = (TextView) findViewById(R.id.BluetoothStatus);


        // If the adapter is null, then Bluetooth is not supported
        if (BluetoothService.isAvailable() == false) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
    }

    protected void initialize(){
        try {
            initController();
            initUI();
        } catch (Exception e){
            Log.v(_TAG, e.getMessage());
        }
    }

    private void initController() throws Exception{
        _mainController = MainController.getInstance();
        _mainController.init(this.getApplicationContext(),mHandler);
        _mainController.enable();

    }

    protected Button            moveUpBtn 		= null;
    protected Button			moveDownBtn 	= null;
    protected Button			moveLeftBtn		= null;
    protected Button			moveRightBtn	= null;
    protected Button			autoBtn 		= null;
    protected Button			exploreBtn 		= null;
    protected Button			refreshBtn 		= null;
    protected Switch			toggleRefreshBtn = null;
    protected Button            f1Btn           = null;
    protected Button            f2Btn           = null;
    protected TextView          lastReceivedBox =null;
    protected TextView          lastSentBox     =null;

    private void initUI() {
        moveUpBtn 			= (Button) findViewById(R.id.MoveUpBtn);
        moveDownBtn 		= (Button) findViewById(R.id.MoveDownBtn);
        moveRightBtn 		= (Button) findViewById(R.id.MoveRightBtn);
        moveLeftBtn 		= (Button) findViewById(R.id.MoveLeftBtn);
        exploreBtn 			= (Button) findViewById(R.id.ExploreBtn);
        refreshBtn 			= (Button) findViewById(R.id.RefreshBtn);
        toggleRefreshBtn 	= (Switch) findViewById(R.id.ToggleRefreshAutoBtn);
        autoBtn 			= (Button) findViewById(R.id.AutoBtn);
        f1Btn               = (Button) findViewById(R.id.f1Btn);
        f2Btn               = (Button) findViewById(R.id.f2Btn);
        lastReceivedBox     = (TextView) findViewById(R.id.ReceivedMessage);
        lastSentBox         = (TextView) findViewById(R.id.SentMessage);

        moveUpBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.MOVE_UP_FUNC,new String[]{KeyMapper.MOVE_UP_FUNC + ""});

            }
        });

        moveDownBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.MOVE_DOWN_FUNC,new String[]{KeyMapper.MOVE_DOWN_FUNC + ""});
            }
        });

        moveRightBtn.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.MOVE_RIGHT_FUNC,new String[]{KeyMapper.MOVE_RIGHT_FUNC + ""});
            }
        });

        moveLeftBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.MOVE_LEFT_FUNC,new String[]{KeyMapper.MOVE_LEFT_FUNC + ""});
            }
        });


        exploreBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.EXPLORE_FUNC,new String[]{});
            }
        });

        refreshBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.REFRESH_FUNC,new String[]{});
            }
        });

        toggleRefreshBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                boolean buttonOn = toggleRefreshBtn.isChecked();
                _mainController.callFunc(KeyMapper.TOGGLE_AUTO_REFRESH_FUNC, buttonOn);
            }
        });

        autoBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.AUTO_FUNC,new String[]{});
            }
        });

        f1Btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.F1_FUNC,new String[]{});
            }
        });

        f2Btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                _mainController.callFunc(KeyMapper.F2_FUNC,new String[]{});
            }
        });

        //TODO: demo mazeholder. To be deleted
        _mainController.getMazePanelController()
                .bind((FrameLayout) findViewById(R.id.MazePlaceHolder));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent serverIntent = null;
        switch (item.getItemId()) {
            case R.id.connect_scan:
                // Launch the DeviceListActivity to see devices and do scan
                serverIntent = new Intent(this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                return true;

            case R.id.set_config:
                //Set configuration
                serverIntent = new Intent(this, PreferenceSetting.class);
                startActivityForResult(serverIntent, REQUEST_SET_CONFIGURATION);
                return true;

            case R.id.tilting_control:
                //change control mode
                tiltSensingMode = _mainController.toggleTiltSensor(); //toggleSensorService();
                break;
        }
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();

        String tiltSenseText;
        if (tiltSensingMode == true){
            tiltSenseText = getResources().getString(R.string.disableTiltSensing);
        } else
            tiltSenseText = getResources().getString(R.string.enableTiltSensing);
        //scan button
        menu.add(Menu.NONE, R.id.connect_scan, Menu.NONE, R.string.connect)
                .setIcon(android.R.drawable.ic_menu_search)
                .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM|MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        menu.add(Menu.NONE, R.id.set_config, Menu.NONE, R.string.setConfig);

        //toggle tilt sensing mode
        menu.add(Menu.NONE,R.id.tilting_control,Menu.NONE,tiltSenseText);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(_TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupBluetoothService() will then be called during onActivityResult
        if (!BluetoothService.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mChatService == null)
                setupBluetoothService();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(_TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }

        //display maze
        //TODO: debug
        _mainController.getMazePanelController().startPreviewMaze();
    }

    private void setupBluetoothService() {
        Log.d(_TAG, "setupBluetoothService()");

        // Initialize the BluetoothService to perform bluetooth connections
        mChatService = new BluetoothService(this, mHandler);
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(_TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(_TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();
        if(D) Log.e(_TAG, "--- ON DESTROY ---");
    }

    /*
    private void ensureDiscoverable() {
        if(D) Log.d(_TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }
*/
    /**
     * Sends a message.
     * @param message  A string of text to send.
     */

    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        mChatService.sendMessage(message);
    }

    public Handler getHandler(){
        return mHandler;
    }

    // The Handler that gets information back from the BluetoothService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if(D) Log.i(_TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            bluetoothStatus.setText("Connected");
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            bluetoothStatus.setText("Connecting");
                            break;
                        case BluetoothService.STATE_LISTEN:
                            bluetoothStatus.setText("Disconnected");
                    }
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    lastSentBox.setText(trimMessage(writeMessage));
                    mChatService.sendMessage(writeMessage);
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.v(_TAG,"MESSAGE_READ: "+readMessage);
                    lastReceivedBox.setText(trimMessage(readMessage));
                    _mainController.onServerResponse(new String[]{readMessage});
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), "Connected to "
                                 + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_ROBOT_STATE_CHANGE:
                    byte[] textBuf = (byte[]) msg.obj;
                    String text = new String(textBuf);
                    robotStatus.setText(text);
                    break;
            }
        }
    };

    private String trimMessage(String message) {
        int MAX_WORD = 400;

        if (message.length()<MAX_WORD)
            return message;

        return message.substring(0,MAX_WORD)+ "...";
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(_TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Attempt to connect to the device address
                    mChatService.connect(address);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupBluetoothService();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d(_TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            case REQUEST_SET_CONFIGURATION:
                if (resultCode == Activity.RESULT_OK) {
                    //remap key on changes
                    _mainController.remappingKeys();
                }
                break;
        }
    }

}