package com.mdpgroup4.controller;

import com.mdpgroup4.robot.NavControllerType;
import com.mdpgroup4.robot.Robot;

public class DefaultSetting {
	public static final NavControllerType NAGIVATION_CONTROLLER_TYPE = NavControllerType.BASIC;
	public static final Robot.Direction ROBOT_DIRECTION = Robot.Direction.DIRECTION_N;
	public static final int ROBOT_X = 3;
	public static final int ROBOT_Y = 4;
	public static final int MAZE_WIDTH 	= 15;
	public static final int MAZE_HEIGHT = 20;
    public static boolean AUTO_REFRESH_MAZE = true;
}
