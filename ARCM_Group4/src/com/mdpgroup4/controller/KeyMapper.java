package com.mdpgroup4.controller;

import java.util.HashMap;

import com.mdpgroup4.activities.PreferenceSetting;

public final class KeyMapper {
    public static final int MOVE_UP_FUNC = 0;
    public static final int MOVE_DOWN_FUNC = 1;
    public static final int MOVE_LEFT_FUNC = 2;
    public static final int MOVE_RIGHT_FUNC = 3;
    public static final int EXPLORE_FUNC = 4;
    public static final int AUTO_FUNC = 5;
    public static final int REFRESH_FUNC = 6;
    public static final int TOGGLE_AUTO_REFRESH_FUNC = 7;
    public static final int F1_FUNC = 10;
    public static final int F2_FUNC = 11;

    public static final int TOGGLE_TILT_SENSOR = 12;
    public static final int AUTO_REFRESH_ON = 13;
    public static final int AUTO_REFRESH_OFF = 14;

    private static HashMap<Integer,String> mapper = new HashMap<Integer, String>();

    public KeyMapper(){

    }

    public static void setDefaultMap(){

        String config_1 = PreferenceSetting.getPref("text_0", MainController.getContext());
        String config_2 = PreferenceSetting.getPref("text_1", MainController.getContext());

        mapper.put(MOVE_UP_FUNC, "a");
        mapper.put(MOVE_DOWN_FUNC, "d");
        mapper.put(MOVE_LEFT_FUNC, "b");
        mapper.put(MOVE_RIGHT_FUNC, "c");

        //unformatted value
//        mapper.put(MOVE_UP_FUNC, "w");
//        mapper.put(MOVE_DOWN_FUNC, "s");
//        mapper.put(MOVE_LEFT_FUNC, "a");
//        mapper.put(MOVE_RIGHT_FUNC, "d");
        mapper.put(EXPLORE_FUNC, "e");
        mapper.put(AUTO_FUNC, "s");
        mapper.put(REFRESH_FUNC, "u");
        mapper.put(AUTO_REFRESH_ON, "on");
        mapper.put(AUTO_REFRESH_OFF, "off");
        mapper.put(F1_FUNC, config_1);
        mapper.put(F2_FUNC, config_2);
    }



    /**
     * get key value
     * @param keyId
     * @return
     */
    public static String getValue(int keyId){
        String value = null;
        switch (keyId){
            //command to adruino
            case MOVE_RIGHT_FUNC:case MOVE_UP_FUNC:
            case MOVE_LEFT_FUNC:case MOVE_DOWN_FUNC:
                value = "{\"cmd\":\"ard\",\"ard\":\""+getUnformattedValue(keyId)+"\"}";
                break;
            //command to PC
            case AUTO_REFRESH_ON:case AUTO_REFRESH_OFF:
            case REFRESH_FUNC:case F1_FUNC:case F2_FUNC:
                    value = "{\"cmd\":\""+getUnformattedValue(keyId)+"\"}";
                break;
            default:
                value = "{\"cmd\":\""+getUnformattedValue(keyId)+"\"}";
        }
        return value;
    }

    /**
     * get unformatted value
     * @param keyId
     * @return
     */
    public static String getUnformattedValue(int keyId){
        return mapper.get(keyId);
    }


    /**
     * Add new key
     * @param keyId
     * @param value
     * @return
     */
    public static String add(int keyId,String value){
        mapper.put(keyId,value);
        return value;
    }

    /**
     * get all key map
     */
    public static HashMap<Integer,String> getMapper(){
        return mapper;
    }

}
