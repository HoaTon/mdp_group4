package com.mdpgroup4.controller;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.mdpgroup4.activities.MainActivity;
import com.mdpgroup4.maze.Maze;
import com.mdpgroup4.robot.Robot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainController implements PanelController{

    private boolean enable;

    private static final String					_TAG	= "com.mdpgroup4.controller.MainController" ;

    private MazePanelController					_mazePanelController = null;
    private NavPanelController					_navPanelController = null;
    private RemoteSystemTracker					_systemTracker = null;
    private static MainController               _self = null;
    private KeyMapper                           _keyMapper = null;
    private static Context                      _context = null;
    private Handler                             _handler = null;

    private MainController() {

    }

    public static MainController getInstance(){

        if (_self == null){
            _self = new MainController();
        }
        return _self;
    }

    public MazePanelController getMazePanelController() {
        return _mazePanelController;
    }

    public void callFunc(int func, String[] args) {
        //TODO: add detail functions
        if (!this.isEnable()){
            Log.w(_TAG, "callFunc(): MainController is disabled");
            return;
        }

        if (func == -1){
            Log.w(_TAG, "callFunc(): Unknown function");
            return;
        }

        switch (func){
            case KeyMapper.MOVE_UP_FUNC:case KeyMapper.MOVE_DOWN_FUNC:
            case KeyMapper.MOVE_LEFT_FUNC:case KeyMapper.MOVE_RIGHT_FUNC:
                _navPanelController.callFunc(func,args);
                break;
            case KeyMapper.EXPLORE_FUNC:
                send(KeyMapper.getValue(KeyMapper.EXPLORE_FUNC));
                break;
            case KeyMapper.AUTO_FUNC:
                send(KeyMapper.getValue(KeyMapper.AUTO_FUNC));
                break;
            case KeyMapper.F1_FUNC:
                send(KeyMapper.getValue(KeyMapper.F1_FUNC));
                Log.v(_TAG, "callFunc(): send key "+KeyMapper.getValue(KeyMapper.F1_FUNC));
                break;
            case KeyMapper.F2_FUNC:
                send(KeyMapper.getValue(KeyMapper.F2_FUNC));
                Log.v(_TAG, "callFunc(): send key "+KeyMapper.getValue(KeyMapper.F2_FUNC));
                break;
            case KeyMapper.REFRESH_FUNC:
                send(KeyMapper.getValue(KeyMapper.REFRESH_FUNC));
                Log.v(_TAG, "callFunc(): send key" + KeyMapper.getValue(KeyMapper.REFRESH_FUNC));
                break;
            default:
                return;
        }
    }

    @Override
    public void callFunc(int func, boolean arg) {
        switch (func){
            case KeyMapper.TOGGLE_AUTO_REFRESH_FUNC:
            _mazePanelController.callFunc(func, arg);
            break;
        }
    }

    public void send(String msg) {
        try {
            // Share the sent message back to the UI Activity
            _handler.obtainMessage(MainActivity.MESSAGE_WRITE, -1, -1,
                    msg.getBytes()).sendToTarget();
        } catch (Exception e) {
            Log.e(_TAG, "Exception during sending message" + e.getMessage());
        }
    }
    public void init(Context context, Handler handler){
        _context = context;
        _handler = handler;

        Log.v(_TAG, "initializing system controllers");

        remappingKeys();

        _systemTracker			= new RemoteSystemTracker();

        _mazePanelController 	= MazePanelController.getInstance();
        _mazePanelController.bind(getMaze());
        _mazePanelController.bind(getRobot());

        _navPanelController 	= NavPanelController.getInstance();
        _navPanelController.bind(getRobot());
    }

    public void remappingKeys() {
        KeyMapper.setDefaultMap();
    }

    protected static Context getContext(){
        return _context;
    }

    public boolean isEnable(){
        return enable;
    }

    public void enable() {
        Log.v(_TAG, "enable(): Enabling Main controller");
        enable = true;
    }

    public void disable() {
        Log.v(_TAG, "disable(): Disabling Main controller");
        enable = false;
    }

    /**
     * parse server response and update system data
     * @param args
     */
    public void onServerResponse(String[] args){
        if (args.length==0)
            return;

        //TODO: debug. To be deleted.
        //TODO: debug 

        if (args[0].equalsIgnoreCase("fwd"))
            _systemTracker.updateRobot(4,10,null,Robot.Direction.DIRECTION_N);

        else if (args[0].equalsIgnoreCase("tbk"))
            _systemTracker.updateRobot(12,15,null,Robot.Direction.DIRECTION_E);

        else if (args[0].equalsIgnoreCase("tlf"))
            _systemTracker.updateRobot(1,1,null,Robot.Direction.DIRECTION_S);

        else if (args[0].equalsIgnoreCase("trg"))
            _systemTracker.updateRobot(15,3,null,Robot.Direction.DIRECTION_W);
        //TODO: add response processing

        try {
            JSONObject obj =  new JSONObject(args[0]);

            //jsonObj.getJSONObject()
            if (obj.has("map")){

                char[] map = obj.getString("map").toCharArray();    //not safe. Will not work if input have negative number

                int mazeWidth = _systemTracker.getMaze().getWidth();
                int mazeHeight = _systemTracker.getMaze().getHeight();
                int[][] states = new int[mazeHeight][mazeWidth];
                int pointer = 0;
                for (int i=0; i<mazeWidth; i++){
                    int j=0;
                    while (j<mazeHeight){
                        states[j][i] = Character.getNumericValue(map[pointer]);
                        pointer++;
                        j++;
                    }
                }

                try{
                    _systemTracker.updateMaze(states);
                } catch (Exception e){
                    Log.e(_TAG,"callFunc(): "+ e.getMessage());
                    _handler.obtainMessage(MainActivity.MESSAGE_TOAST,"Failed to update maze tracker");
                }

            }


            //status
            if (obj.has("status")){
                String subStr       = obj.getString("status");
                JSONObject subObj   = new JSONObject(subStr);
                JSONArray position  = subObj.getJSONArray("p");
                int x            = position.getInt(0);
                int y            = position.getInt(1);
                int state       = Integer.parseInt(subObj.getString("s"));
                int direction   = Integer.parseInt(subObj.getString("d"));

                Robot.State s = null;
                Robot.Direction d = null;

                switch (state){
                    case -1:
                        s  = Robot.State.NOT_READY;
                        break;
                    case 0:
                        s   = Robot.State.IDLE;
                        break;
                    case 1:
                        s   = Robot.State.MOVING;
                        break;
                    case 2:
                        s   = Robot.State.TURNING;
                        break;
                    default:
                        s   = Robot.State.IDLE;

                }

                switch (direction){
                    case 3:
                        d = Robot.Direction.DIRECTION_N;
                        break;
                    case 4:
                        d = Robot.Direction.DIRECTION_S;
                        break;
                    case 0:
                        d = Robot.Direction.DIRECTION_W;
                        break;
                    case 1:
                        d = Robot.Direction.DIRECTION_E;
                        break;
                    default:
                        d = Robot.Direction.DIRECTION_N;
                }

                try{
                    _systemTracker.updateRobot(x, y, s, d);
                } catch (Exception e){
                    Log.e(_TAG,"callFunc(): "+ e.getMessage());
                    _handler.obtainMessage(MainActivity.MESSAGE_TOAST,"Failed to update robot tracker");
                }

                _handler.obtainMessage(MainActivity.MESSAGE_ROBOT_STATE_CHANGE, s._desc.getBytes()).sendToTarget();
            }

        } catch (JSONException e) {
            Log.e(_TAG,"callFunc(): " + e.getMessage());
            e.printStackTrace();
        }

        _mazePanelController.onDataChanged();


    }

    public boolean toggleTiltSensor(){

        return _navPanelController.toggleTiltSensor();
    }
    /**
     * get maze tracking object
     * @return
     */
    public Maze getMaze(){
        return _systemTracker.getMaze();
    }

    /**
     * get robot tracking object
     */
    public Robot getRobot(){
        return _systemTracker.getRobot();
    }

    class RemoteSystemTracker {

        protected Robot _robot;
        protected Maze  _maze;

        private RemoteSystemTracker(){
            _robot 	= new Robot();
            _maze 	= new Maze();
        }

        public Robot getRobot(){
            return _robot;
        }

        public Maze getMaze(){
            return _maze;
        }

        public void updateRobot(int x, int y, Robot.State state, Robot.Direction dir){
            _robot.setPosX(x);
            _robot.setPosY(y);
            if (state != null || dir != null){
                _robot.setState(state);
                _robot.setDirection(dir);
            }
            _robot.setValid(false);
        }

        public void updateMaze(int[][] states){ //y is y-axix, x is x-axis
            for (int y=0;y<states.length;y++)
                for (int x=0;x<states[y].length;x++){
                    _maze.setGridState(x,y,states[y][x]);
                }
            _maze.setValid(false);
        }
    }
}
