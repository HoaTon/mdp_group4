package com.mdpgroup4.controller;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.mdpgroup4.maze.BasicMazeHolder;
import com.mdpgroup4.maze.Maze;
import com.mdpgroup4.maze.MazeHolder;
import com.mdpgroup4.robot.Robot;

public class MazePanelController implements PanelController{
	
	private final String 		_TAG 		= "com.mdpgroup4.controller.MazePanelController";
	
	private boolean             enable = false;
    private boolean             _autoRefresh;
	private MazeHolder			_mazeHolder = null;
	
	private static MazePanelController self = null;
	
	private MazePanelController(){
		initialize();		
	}
	
	//TODO: make this initialization customizable 
	private void initialize() {
		_mazeHolder 	= new BasicMazeHolder(MainController.getContext());
        _autoRefresh    = DefaultSetting.AUTO_REFRESH_MAZE;
	}

    /**
     * binding maze tracker to controller
     * @param maze  - maze
     */
    public void bind(Maze maze){
        _mazeHolder.bind(maze);
    }

    /**
     * binding robot tracker to controller
     * @param robot  - robot
     */
    public void bind(Robot robot){
        _mazeHolder.bind(robot);
    }

    /**
     * binding maze placeholder to controller
     * @param frame  - placeholder
     */
	public void bind(FrameLayout frame){
		_mazeHolder.bind(frame);
	}

    /**
     * adding maze view into UI
     */
	public void startPreviewMaze(){
		_mazeHolder.preview();
	}
    public static MazePanelController getInstance(){
		if (self == null)
			self = new MazePanelController();
		return self;
	}

	public void callFunc(int func,String args[]) {
		switch (func){
		case KeyMapper.REFRESH_FUNC:
			Log.v(_TAG, "callFunc(): send key "+KeyMapper.getValue(KeyMapper.REFRESH_FUNC));
			break;
		default:
			break;
		}
		
	}

    @Override
    public void callFunc(int func, boolean arg) {
        switch (func){
        case KeyMapper.TOGGLE_AUTO_REFRESH_FUNC:
            _autoRefresh = arg;
            if (_autoRefresh)
                MainController.getInstance().send(KeyMapper.getValue(KeyMapper.AUTO_REFRESH_ON));
            else
                MainController.getInstance().send(KeyMapper.getValue(KeyMapper.AUTO_REFRESH_OFF));

            Log.v(_TAG, "callFunc(): toogle auto refresh function");
            break;


        }
    }

    /**
     * update maze view on data update
     */
    public void onDataChanged(){
        _mazeHolder.onDataChanged();
    }

}
