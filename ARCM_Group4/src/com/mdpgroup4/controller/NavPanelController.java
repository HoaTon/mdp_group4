package com.mdpgroup4.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.mdpgroup4.robot.*;

class NavPanelController implements PanelController{
	
	private boolean enable = false;
	private static final String					_TAG			= "com.mdpgroup4.controller.NavPanelController" ;
	private NavigationController				_navController 	= null;
	private Context                             _context = null;
	private static NavPanelController           _self = null;
    private NavControllerType                   _controllerType;
    protected Robot                             robot;
	private NavPanelController(){
        _context = MainController.getContext();
		switch (DefaultSetting.NAGIVATION_CONTROLLER_TYPE){
		case BASIC:
			_navController = new BasicNavController();
            _controllerType = NavControllerType.BASIC;
			break;
        case ADVANCED:
            _navController = new TiltSensingController(_context,_handler);
            _controllerType = NavControllerType.ADVANCED;
            break;
		}
	}
	
	public static NavPanelController getInstance(){
		if (_self == null)
			_self = new NavPanelController();
		return _self;
	}

    public void bind(Robot robot){
        this.robot = robot;
    }

	@Override
	public void callFunc(int func,String args[]) {
        //not in basic control mode
        if (!(_navController instanceof BasicNavController)){
            return;
        }

        String cmd = _navController.getControlCmd(args);
        try {
            MainController.getInstance().send(cmd);
            Log.v(_TAG, "callFunc(): send key "+ cmd);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG,"callFunc(): "+e.getMessage());
        }
    }

    @Override
    public void callFunc(int func, boolean arg) {
        //dummy
    }

    private final Handler _handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SensorService.MESSAGE_SENSOR:
                    Integer code = (Integer) msg.obj;
                    Log.v(_TAG,"MESSAGE_SENSOR: "+code.toString());
                    try {
                        MainController.getInstance().send( _navController.getControlCmd(code));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    public boolean toggleTiltSensor() {
        boolean toggleOn = false;
        if (_controllerType == NavControllerType.BASIC) {
            _navController = new TiltSensingController(_context,_handler);
            toggleOn = true;
            _controllerType = NavControllerType.ADVANCED;
            _navController.getControlCmd(null);
        } else {
            _navController.getControlCmd(null);
            _navController = new BasicNavController();
            _controllerType = NavControllerType.BASIC;
        }

        return toggleOn;
    }
}

