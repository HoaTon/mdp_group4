package com.mdpgroup4.networkservice;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;

/**
 * Created by Hoa_2 on 3/4/14.
 */
public class BluetoothService{

    public static final int STATE_NONE = BluetoothServiceImpl.STATE_NONE;
    public static final int STATE_LISTEN = BluetoothServiceImpl.STATE_LISTEN;
    public static final int STATE_CONNECTING = BluetoothServiceImpl.STATE_CONNECTING;
    // connection
    public static final int STATE_CONNECTED = BluetoothServiceImpl.STATE_CONNECTED;
    // device
    private static BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothServiceImpl    _bluetoothService = null;

    public BluetoothService(Context context, Handler handler){
        _bluetoothService = new BluetoothServiceImpl(context,handler);
    }


    public int getState() {
        return _bluetoothService.getState();
    }

    public void start() {
        _bluetoothService.start();
    }

    public void stop() {
        _bluetoothService.stop();
    }

    public void connect(String address) {
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        _bluetoothService.connect(device);
    }

    public void sendMessage(String message){
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothService to write
            byte[] send = message.getBytes();
            _bluetoothService.write(send);

            // Reset out string buffer to zero and clear the edit text field
            //mOutStringBuffer.setLength(0);
        }
    }

    public static boolean isAvailable(){
        // Get local Bluetooth adapter
        if (mBluetoothAdapter == null)
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null)
            return false;
        return true;
    }

    public static boolean isEnabled(){
        return mBluetoothAdapter.isEnabled();
    }
}
